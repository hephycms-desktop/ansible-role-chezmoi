Role Name
=========

Install [chezmoi](https://www.chezmoi.io)

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.chezmoi

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
